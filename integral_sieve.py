import numpy as np
import math

def get_prev_x(vals, x):
    for idx, val in enumerate(vals):
        if idx >= x:
            return idx-1
    else:
        return idx

def get_prev_xidx(vals, x):
    for idx, val in enumerate(vals):
        if val > x:
            return idx-1
        elif val == x:
            return idx
    else:
        return idx

def expand_cdf(ydata, xdata, xs):
    y_new = []
    for x in xs:
        # assumes the provided xs are in sorted order
        y_new.append(ydata[get_prev_x(ydata, x)])
    return y_new


def mystepintegrate(cdf, cdf_xvals, xmax):
    count = 0
    curr_y = cdf[count]
    integral = 0

    for idx, x in enumerate(cdf):
        if (idx == 0):
            continue
        count += 1
        curr_y = cdf[idx]
        integral += curr_y*(cdf_xvals[idx] - cdf_xvals[idx-1])

    if (cdf_xvals[-1] < xmax):
        integral += curr_y*(xmax - cdf_xvals[idx])
    

    return integral

def stepintegrate(cdf, xvals):
    integral = 0

    for idx, val in enumerate(xvals):
        if (idx == 0):
            continue
        integral += cdf[idx-1]*(xvals[idx] - xvals[idx-1])

    return integral

def sieve(nsolv, solvs, cdf_xvals, all_cdfs, xs):

    half_x = xs[-1]/2
    x3_4ths = 3*xs[-1]/4
    full_x = xs[-1]

    x150pc = xs[-1]*1.5    

    best_lists = []

    solv_vals = []
    # will also want to add one more x-value about 50% ahead of top value eventually..
    half_integrals = []
    for idx in range(nsolv):
        half_x_idx = get_prev_xidx(cdf_xvals[idx], half_x)
        half_integrals.append(mystepintegrate(all_cdfs[idx][0:half_x_idx], cdf_xvals[idx][0:half_x_idx], half_x))

    
    cutoff = 6   # will be some rounded fraction of the total number of solvers
    
    tmp1, tmp2, tmp3, tmp4 = zip(*sorted(zip(half_integrals, solvs, cdf_xvals, all_cdfs)))

    tmp1 = tmp1[-cutoff::]
    tmp2 = tmp2[-cutoff::]
    solvs = tmp2
    best_lists.append(solvs)
    cdf_xvals = tmp3[-cutoff::]
    my_cdfs = tmp4[-cutoff::]
    
    nsolv = cutoff
    int_3_4ths = []
    for idx in range(nsolv):
        x3_4ths_idx = get_prev_xidx(cdf_xvals[idx], x3_4ths)
        int_3_4ths.append(mystepintegrate(my_cdfs[idx][0:x3_4ths_idx], cdf_xvals[idx][0:x3_4ths_idx], x3_4ths))

    #best_idxs = np.argsort(int_3_4ths, axis=0)
    #int_3_4ths = np.sort(int_3_4ths, axis=0)

    #solv_3_4ths = [best_half_length_solvs[idx] for idx in best_idxs]

    print("Metric based on integration up to the 3/4th-point of max(x):")
    cutoff = 3
    tmp1, tmp2, tmp3, tmp4 = zip(*sorted(zip(int_3_4ths, solvs, cdf_xvals, my_cdfs)))
    
    tmp1 = tmp1[-cutoff::]
    tmp2 = tmp2[-cutoff::]
    solvs = tmp2
    best_lists.append(solvs)
    cdf_xvals = tmp3[-cutoff::]
    my_cdfs = tmp4[-cutoff::]
    
    nsolv = cutoff
    int_full_len = []
    for idx in range(nsolv):
        full_x_idx = get_prev_xidx(cdf_xvals[idx], full_x)
        int_full_len.append(mystepintegrate(my_cdfs[idx][0:full_x_idx], cdf_xvals[idx][0:full_x_idx], full_x))

    tmp1, tmp2, tmp3, tmp4 = zip(*sorted(zip(int_full_len, solvs, cdf_xvals, my_cdfs)))

    tmp1 = tmp1[-cutoff::]
    tmp2 = tmp2[-cutoff::]
    solvs = tmp2
    best_lists.append(solvs)
    cdf_xvals = tmp3[-cutoff::]
    my_cdfs = tmp4[-cutoff::]

    # this is where things may break...

    int_full_len = []
    for idx in range(nsolv):
        x150pc_idx = get_prev_xidx(cdf_xvals[idx], x150pc)
        int_full_len.append(mystepintegrate(my_cdfs[idx][0:x150pc_idx], cdf_xvals[idx][0:x150pc_idx], x150pc))

    tmp1, tmp2, tmp3, tmp4 = zip(*sorted(zip(int_full_len, solvs, cdf_xvals, my_cdfs)))
    cutoff = 1
    tmp1 = tmp1[-cutoff::]
    tmp2 = tmp2[-cutoff::]
    solvs = tmp2
    best_lists.append(solvs)
    cdf_xvals = tmp3[-cutoff::]
    my_cdfs = tmp4[-cutoff::]
    
    best_solver = tmp2[0]

    return (best_lists)
