#!/bin/bash

include ${PETSC_DIR}/${PETSC_ARCH}/lib/petsc/conf/petscvariables

CAT:=$(firstword $(shell which cat))
CHMOD:=$(firstword $(shell which chmod))
LN:=$(firstword $(shell which ln))
LS:=$(firstword $(shell which ls))
ARREPFLAGS:="-rcu"
RMDIR:=$(firstword $(shell which rmdir))
AWK:=$(firstword $(shell which awk))
HEAD:=$(firstword $(shell which head))
TAIL:=$(firstword $(shell which tail))
WC:=$(firstword $(shell which wc))

TAOSTER=$(PWD)
SIFDECODE=$(TAOSTER)/SIFDecode
CUTEST=$(TAOSTER)/CUTEst
VERSION=all.petsc.mpi
PRECIS=$(PETSC_PRECISION)

SIFOBJ=$(SIFDECODE)/objects/$(VERSION)
SIFMOD=$(SIFDECODE)/modules/$(VERSION)

CUTEOBJ=$(CUTEST)/objects/$(VERSION)
CUTEMOD=$(CUTEST)/modules/$(VERSION)

.PHONY: all sifdecode cutest

all: sifdecode cutest
	@echo " Downloading sif files. This might take a while..."
	@if cd $(TAOSTER)/sif; then git pull; else cd $(TAOSTER) && git clone https://bitbucket.org/optrove/sif ./sif; fi
	@echo " Download complete!"
	@echo "-------------------------------"
	@echo " Make sure to set the following environment variables:"
	@echo "    export SIFDECODE=$(SIFDECODE)"
	@echo "    export CUTEST=$(CUTEST)"
	@echo "    export ARCHDEFS=$(CUTEST)"
	@echo "    export MYARCH=$(VERSION)"
	@echo "    export MASTSIF=$(TAOSTER)/sif"
	@echo "==================================================="

sifdecode:
	@$(CP) $(TAOSTER)/all.petsc.sifdecode $(SIFDECODE)/makefiles/$(VERSION)
	$(eval SYSFILE=$(SIFDECODE)/bin/sys/$(VERSION))
	@echo "RM=\"$(RM)\""                                                                                     >  $(SYSFILE)
	@echo "MAKE=\"$(MAKE)\""                                                                                 >> $(SYSFILE)
	@echo "CAT=\"$(CAT)\""                                                                                   >> $(SYSFILE)
	@echo "SED=\"$(SED)\""                                                                                   >> $(SYSFILE)
	@echo "MV=\"$(MV)\""                                                                                     >> $(SYSFILE)
	@echo "CP=\"$(CP)\""                                                                                     >> $(SYSFILE)
	@echo "LS=\"$(LS)\""                                                                                     >> $(SYSFILE)
	@echo "LN=\"$(LN)\""                                                                                     >> $(SYSFILE)
	@echo "GREP=\"$(GREP)\""                                                                                 >> $(SYSFILE)
	@echo "AWK=\"$(AWK)\""                                                                                   >> $(SYSFILE)
	@echo "HEAD=\"$(HEAD)\""                                                                                 >> $(SYSFILE)
	@echo "TAIL=\"$(TAIL)\""                                                                                 >> $(SYSFILE)
	@echo "FORTRAN=\"$(FC)\""                                                                                >> $(SYSFILE)
	@echo "FFLAGS=\" -I$(SIFDECODE)/modules/$(VERSION)/double -fopenmp\""                                    >> $(SYSFILE)
	@echo "PROBFLAGS=\" -I$(SIFDECODE)/modules/$(VERSION)/double -c -fPIC -O -ffixed-form\""                 >> $(SYSFILE)
	@echo "BLAS=\"\""                                                                                        >> $(SYSFILE)
	@echo "LAPACK=\"\""                                                                                      >> $(SYSFILE)
	$(eval OBJDIR=$(SIFDECODE)/objects/$(VERSION))
	@if [ ! -f $(OBJDIR)/double ]; then $(MKDIR) $(OBJDIR)/double; fi
	$(eval MODDIR=$(SIFDECODE)/modules/$(VERSION))
	@if [ ! -f $(MODDIR)/double ]; then $(MKDIR) $(MODDIR)/double; fi
	@echo "PETSc based configuration for all architectures and systems" > $(SIFDECODE)/versions/$(VERSION)
	@echo "==================================================="
	@echo " SIFDecode double precision"
	@echo "-------------------------------"
	@echo " compiling with the comand"
	@echo " $(MAKE) -s -f $(SIFDECODE)/makefiles/$(VERSION) all PRECIS=double PWD=$(SIFDECODE)/src SIFDECODE=$(SIFDECODE)"
	@cd $(SIFDECODE)/src && $(MAKE) -s -f $(SIFDECODE)/makefiles/$(VERSION) all PRECIS=double PWD=$(SIFDECODE)/src SIFDECODE=$(SIFDECODE)
	@echo " SIFDecode successfully installed"
	@echo "==================================================="
	
cutest:
	@$(CP) $(TAOSTER)/all.petsc.cutest $(CUTEST)/makefiles/$(VERSION)
	$(eval SYSFILE=$(CUTEST)/bin/sys/$(VERSION))
	@echo "RM=\"$(RM)\""                                                                                     >  $(SYSFILE)
	@echo "MAKE=\"$(MAKE)\""                                                                                 >> $(SYSFILE)
	@echo "CAT=\"$(CAT)\""                                                                                   >> $(SYSFILE)
	@echo "SED=\"$(SED)\""                                                                                   >> $(SYSFILE)
	@echo "MV=\"$(MV)\""                                                                                     >> $(SYSFILE)
	@echo "CP=\"$(CP)\""                                                                                     >> $(SYSFILE)
	@echo "LS=\"$(LS)\""                                                                                     >> $(SYSFILE)
	@echo "LN=\"$(LN)\""                                                                                     >> $(SYSFILE)
	@echo "GREP=\"$(GREP)\""                                                                                 >> $(SYSFILE)
	@echo "AWK=\"$(AWK)\""                                                                                   >> $(SYSFILE)
	@echo "HEAD=\"$(HEAD)\""                                                                                 >> $(SYSFILE)
	@echo "TAIL=\"$(TAIL)\""                                                                                 >> $(SYSFILE)
	@echo "FORTRAN=\"$(FC)\""                                                                                >> $(SYSFILE)
	@echo "FFLAGS=\" -I$(CUTEST)/modules/$(VERSION)/$(PRECIS) -fopenmp\""                                    >> $(SYSFILE)
	@echo "PROBFLAGS=\" -I$(CUTEST)/modules/$(VERSION)/$(PRECIS) -c -fPIC -O -ffixed-form\""                 >> $(SYSFILE)
	@echo "BLAS=\"\""                                                                                        >> $(SYSFILE)
	@echo "LAPACK=\"\""                                                                                      >> $(SYSFILE)
	$(eval OBJDIR=$(CUTEST)/objects/$(VERSION))
	@if [ ! -f $(OBJDIR)/$(PRECIS) ]; then $(MKDIR) $(OBJDIR)/$(PRECIS); fi
	$(eval MODDIR=$(CUTEST)/modules/$(VERSION))
	@if [ ! -f $(MODDIR)/$(PRECIS) ]; then $(MKDIR) $(MODDIR)/$(PRECIS); fi
	@echo "PETSc based configuration for all architectures and systems" > $(CUTEST)/versions/$(VERSION)
	@echo "==================================================="
	@echo " CUTEst $(PRECIS) precision"
	@echo "-------------------------------"
	@echo " compiling with the comand"
	@echo " $(MAKE) -s -f $(CUTEST)/makefiles/$(VERSION) all PRECIS=$(PRECIS) PWD=$(CUTEST)/src CUTEST=$(CUTEST)"
	@cd $(CUTEST)/src && $(MAKE) -s -f $(CUTEST)/makefiles/$(VERSION) all PRECIS=$(PRECIS) PWD=$(CUTEST)/src CUTEST=$(CUTEST)
	@echo " CUTEst successfully installed"
	@echo "==================================================="
	
clean:
	@echo " Removing object files and libraries ... "
	@$(RM) -r $(SIFDECODE)/objects/$(VERSION)
	@$(RM) -r $(CUTEST)/objects/$(VERSION)
	@echo " Removing module information files ... "
	@$(RM) -r $(SIFDECODE)/modules/$(VERSION)
	@$(RM) -r $(CUTEST)/modules/$(VERSION)
	@echo " Removing environment information file ... "
	@$(RM) -r $(SIFDECODE)/bin/sys/$(VERSION)
	@$(RM) -r $(CUTEST)/bin/sys/$(VERSION)
	@echo " Removing make information file ... "
	@$(RM) -r $(SIFDECODE)/makefiles/$(VERSION)
	@$(RM) -r $(CUTEST)/makefiles/$(VERSION)
	@echo " Removing version record file ... "
	@$(RM) -r $(SIFDECODE)/versions/$(VERSION)
	@$(RM) -r $(CUTEST)/versions/$(VERSION)

cleanall: clean
	@echo " Removing downloaded sif files ..."
	@$(RM) -rf $(TAOSTER)/sif
