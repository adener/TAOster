import numpy as np
import math

def get_prev_x(vals, x):
    for idx, val in enumerate(vals):
        if idx >= x:
            return idx-1
    else:
        return idx

def get_prev_xidx(vals, x):
    for idx, val in enumerate(vals):
        if val > x:
            return idx-1
        elif val == x:
            return idx
    else:
        return idx

def expand_cdf(ydata, xdata, xs):
    y_new = []
    for x in xs:
        # assumes the provided xs are in sorted order
        y_new.append(ydata[get_prev_x(ydata, x)])
    return y_new

def pairwise_compare(integrals, encumb, cutff):
    cutoff_idx = 0
    # assumes you have a sorted list
    for idx, integral in enumerate(integrals):
        if integral >= cutff*encumb:
            # first time this happens, should return the last index if it isn't 0
            if idx != 0:

                return idx
            else:
                return idx

def mystepintegrate(cdf, cdf_xvals, xmax):
    count = 0
    curr_y = cdf[count]
    integral = 0

    for idx, x in enumerate(cdf):
        if (idx == 0):
            continue
        count += 1
        curr_y = cdf[idx]
        integral += curr_y*(cdf_xvals[idx] - cdf_xvals[idx-1])

    if (cdf_xvals[-1] < xmax):
        integral += curr_y*(xmax - cdf_xvals[idx])
    

    return integral

def sieve(solvs, cdf_xvals, all_cdfs, xs, max_x_frac, cutoff_pc):
    nsolv = len(cdf_xvals)
    max_x = max_x_frac*xs[-1]

    best_lists = []

    solv_vals = []
    # will also want to add one more x-value about 50% ahead of top value eventually..
    integs = []
    for idx in range(nsolv):
        max_x_idx = get_prev_xidx(cdf_xvals[idx], max_x)
        integs.append(mystepintegrate(all_cdfs[idx][0:max_x_idx], cdf_xvals[idx][0:max_x_idx], max_x))
    
    first = 1
    tmp1, tmp2, tmp3, tmp4 = zip(*sorted(zip(integs, solvs, cdf_xvals, all_cdfs)))
    
    encumb = tmp1[-first]
    # get the index in which our sorted list has methods which are within cutoffpc of the best method

    idx_cutoff = pairwise_compare(tmp1, encumb, cutoff_pc)
    tmp1 = tmp1[idx_cutoff::]
    
    tmp2 = tmp2[idx_cutoff::]
    solvs = tmp2
    
    cdf_xvals = tmp3[idx_cutoff::]
    my_cdfs = tmp4[idx_cutoff::]
    
    best_lists = solvs
    
    return (best_lists, cdf_xvals, my_cdfs)
