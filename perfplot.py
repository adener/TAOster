#!/usr/bin/env python3

from __future__ import division
from __future__ import print_function
from builtins import range

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

import numpy as np
import matplotlib.pyplot as plt
import argparse

# problem type
probtype = 'bound'
suffix = 'qn'
labels = []

# configure the parser
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--probtype', type=str, nargs=1, default=[probtype], help='specify the CUTEst problem set')
parser.add_argument('-s', '--suffix', type=str, nargs=1, default=[suffix], help='specify the suffix for the comparison files to be plotted')
parser.add_argument('-l', '--labels', type=str, nargs='*', default=labels, help='specify labels for the legend')
args = parser.parse_args()
probtype = args.probtype[0]
suffix = args.suffix[0]
labels = args.labels

# read the TAO solvers from the header comment of the data file
with open('%s/iters_%s.out'%(probtype,suffix)) as f:
    raw_iters = f.readlines()
solvers = raw_iters[0]
solvers = solvers.strip()
solvers = solvers.split(' ')
solvers = solvers[2:]
# print(solvers)
if len(labels) != len(solvers):
    labels = solvers

# load the data files and get the sizes
iters = np.loadtxt('%s/iters_%s.out'%(probtype,suffix))
fevals = np.loadtxt('%s/fevals_%s.out'%(probtype,suffix))
kspits = np.loadtxt('%s/kspits_%s.out'%(probtype,suffix))
cputime = np.loadtxt('%s/cputime_%s.out'%(probtype,suffix))
# steplen = np.loadtxt('%s/steplen_%s.out'%(probtype,suffix))
nprob, nsolver = np.shape(iters)

nsolver = len(solvers)
print("Plotting %i total problems..."%nprob)


# generate empty sets to store ratios
iters_ratio = np.zeros((nprob, nsolver))
fevals_ratio = np.zeros((nprob, nsolver))
kspits_ratio = np.zeros((nprob, nsolver))
cputime_ratio = np.zeros((nprob, nsolver))
# steplen_ratio = np.zeros((nprob, nsolver))

# loop over problems and compute ratios
eps = np.power(np.finfo(float).eps, 2.0/3.0)
for i in range(nprob):

    iters_ratio[i,:] = np.divide(np.maximum(iters[i,:], np.ones(nsolver, dtype=int)), max(np.nanmin(iters[i,:]), 1.))
    fevals_ratio[i,:] = np.divide(np.maximum(fevals[i,:], np.ones(nsolver, dtype=int)), max(np.nanmin(fevals[i,:]), 1.))
    kspits_ratio[i,:] = np.divide(np.maximum(kspits[i,:], np.ones(nsolver, dtype=int)), max(np.nanmin(kspits[i,:]), 1.))
    cputime_ratio[i,:] = np.divide(np.maximum(cputime[i,:], eps*np.ones(nsolver)), max(np.nanmin(cputime[i,:]), eps))
    # steplen_ratio[i,:] = np.divide(np.maximum(steplen[i,:], np.ones(nsolver, dtype=int)), max(steplen[i,:]))

#### UNCOMMENT BELOW FOR WORKING WITH MORE SOLVERS IN SUBFOLDERS SO THEY HAVE GOOD LEGEND NAMES

# ydata = old_div((np.arange(nprob, dtype=float) + 1.0),float(nprob))
# slv_temp = []
# for solver in solvers:
#     tmp = solver.split('/')
#     if len(tmp)>1:
#         slv_temp.append('%s/%s'%(tmp[-2], tmp[-1]))
#     else:
#         slv_temp.append('%s'%(tmp[-1]))
#
# solvers = slv_temp

# generate y-axis for the performance plots
ydata = np.linspace(0, 1, num=nprob)

# identify failed problem and set their cost to twice the maximum
max_iters = np.nanmax(iters_ratio.flatten())
max_evals = np.nanmax(fevals_ratio.flatten())
max_ksp = np.nanmax(kspits_ratio.flatten())
max_time = np.nanmax(cputime_ratio.flatten())
# min_step = np.nanmin(steplen_ratio.flatten())

# performance with nonlinear iterations
plt.figure(1, figsize=(12, 12))
for i in range(nsolver):
    solver_data = iters_ratio[:,i]
    solver_data = solver_data[~np.isnan(solver_data)]
    solver_data = np.sort(solver_data, axis=0)
    plt.step(np.append(solver_data, max_iters), np.append(ydata[:len(solver_data)], ydata[len(solver_data)-1]), label=labels[i])
plt.xscale('log')
plt.legend(fontsize=16, loc=4)
plt.grid(b=True, which='both', axis='y', linestyle='--', linewidth=0.25, color='k')
plt.ylabel(r"$P_m^c(\pi)$", fontsize=22)
plt.ylim(0, 1)
plt.xlabel(r"$\pi$", fontsize=22)
plt.xlim(1, max_iters)
plt.savefig('%s/iters_%s.eps'%(probtype,suffix))
plt.savefig('%s/iters_%s.png'%(probtype,suffix))

# performance with function/gradient evaluations
plt.figure(2, figsize=(12, 12))
for i in range(nsolver):
    solver_data = fevals_ratio[:,i]
    solver_data = solver_data[~np.isnan(solver_data)]
    solver_data = np.sort(solver_data, axis=0)
    plt.step(np.append(solver_data, max_evals), np.append(ydata[:len(solver_data)], ydata[len(solver_data)-1]), label=labels[i])
plt.xscale('log')
plt.legend(fontsize=16, loc=4)
plt.grid(b=True, which='both', axis='y', linestyle='--', linewidth=0.25, color='k')
plt.ylabel(r"$P_m^c(\pi)$", fontsize=22)
plt.ylim(0, 1)
plt.yticks(fontsize=20)
plt.xticks(fontsize=20)
plt.xlabel(r"$\pi$", fontsize=22)
plt.xlim(1, max_evals)
plt.savefig('%s/fevals_%s.eps'%(probtype,suffix))
plt.savefig('%s/fevals_%s.png'%(probtype,suffix))

# performance with KSP iterations
plt.figure(3, figsize=(12, 12))
for i in range(nsolver):
    solver_data = kspits_ratio[:,i]
    solver_data = solver_data[~np.isnan(solver_data)]
    solver_data = np.sort(solver_data, axis=0)
    plt.step(np.append(solver_data, max_ksp), np.append(ydata[:len(solver_data)], ydata[len(solver_data)-1]), label=labels[i])
plt.xscale('log')
plt.legend(fontsize=16, loc=4)
plt.grid(b=True, which='both', axis='y', linestyle='--', linewidth=0.25, color='k')
plt.ylabel(r"$P_m^c(\pi)$")
plt.ylim(0, 1)
plt.xlabel(r"$\pi$")
plt.xlim(1, max_ksp)
plt.savefig('%s/kspits_%s.eps'%(probtype,suffix))
plt.savefig('%s/kspits_%s.png'%(probtype,suffix))

# plot CPU time ratio
plt.figure(4, figsize=(12, 12))
for i in range(nsolver):
    solver_data = cputime_ratio[:,i]
    solver_data = solver_data[~np.isnan(solver_data)]
    solver_data = np.sort(solver_data, axis=0)
    plt.step(np.append(solver_data, max_time), np.append(ydata[:len(solver_data)], ydata[len(solver_data)-1]), label=labels[i])
plt.xscale('log')
plt.legend(fontsize=16, loc=4)
plt.grid(b=True, which='both', axis='y', linestyle='--', linewidth=0.25, color='k')
plt.ylabel(r"$P_m^c(\pi)$")
plt.ylim(0, 1)
plt.xlabel(r"$\pi$")
plt.xlim(1, max_time)
plt.savefig('%s/cputime_%s.eps'%(probtype,suffix))
plt.savefig('%s/cputime_%s.png'%(probtype,suffix))

# plot CPU time raw
plt.figure(5, figsize=(12, 12))
max_time_raw = np.nanmax(cputime.flatten())
min_time_raw = max(np.nanmin(cputime[cputime != 0]).flatten(), eps)
min_time_raw = 10.**np.floor(np.log10(min_time_raw))
cputime[cputime == 0] = min_time_raw
for i in range(nsolver):
    solver_data = cputime[:,i]
    solver_data = solver_data[~np.isnan(solver_data)]
    solver_data = np.sort(solver_data, axis=0)
    plt.step(np.append(solver_data, max_time_raw), np.append(ydata[:len(solver_data)], ydata[len(solver_data)-1]), label=labels[i])
plt.xscale('log')
plt.legend(fontsize=16, loc=4)
plt.grid(b=True, which='both', axis='y', linestyle='--', linewidth=0.25, color='k')
plt.ylabel(r"$P_m^c(\pi)$")
plt.ylim(0, 1)
plt.xlabel('CPU time (s)')
plt.xlim(min_time_raw, max_time_raw)
plt.savefig('%s/cputime_raw_%s.eps'%(probtype,suffix))
plt.savefig('%s/cputime_raw_%s.png'%(probtype,suffix))

# plot steplength
# plt.figure(6, figsize=(12,12))
# for i in range(nsolver):
#     solver_data = steplen_ratio[:,i]
#     solver_data = solver_data[~np.isnan(solver_data)]
#     solver_data[solver_data == 0] = eps
#     solver_data = np.sort(solver_data, axis=0)[::-1]
#     plt.step(np.append(solver_data, min_step), np.append(ydata[:len(solver_data)], ydata[len(solver_data)-1]), label=labels[i])
# plt.xscale('log')
# plt.legend(fontsize=16, loc=4)
# plt.grid(b=True, which='both', axis='y', linestyle='--', linewidth=0.25, color='k')
# plt.ylabel(r"$P_m^c(\pi)$", fontsize=22)
# plt.ylim(0, 1)
# plt.yticks(fontsize=20)
# plt.xticks(fontsize=20)
# plt.xlabel(r"$\pi$", fontsize=22)
# plt.xlim(1, min_step)
# plt.savefig('%s/steplen_%s.eps'%(probtype,suffix))
# plt.savefig('%s/steplen_%s.png'%(probtype,suffix))
