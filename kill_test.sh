#!/bin/zsh

echo "killing profiler..."
pkill -f profile.py

echo "killing runcutest..."
for KILLPID in `ps -a | grep 'runcutest' | awk ' { print $1;}'`; do 
  kill -9 $KILLPID;
done

echo "killing run_tao..."
for KILLPID in `ps -a | grep 'run_tao' | awk ' { print $1;}'`; do 
  kill -9 $KILLPID;
done

echo "cleaning up..."
rm *.d
rm *.f
rm *.o
rm run_tao
rm -rf run_tao.dSYM