# TAOster

## Toasty hot testing scripts to run TAO solvers on CUTEst problems.

Run the suite of problems using `profile.py`. Analyze the results into plottable files using `parse.py`. Generate 
CUTEst performance profile plots using `perfplot.py`. Voila!

### Dependencies
1. Python 2.7
2. Numpy
3. Matplotlib
4. PETSc/TAO

Tested and validated on a 2017 Macbook Pro using Homebrew Python@2 and PETSc-built MPI compilers based on Apple 
clang Homebrew gfortran.

**Note:** The up-to-date TAO-CUTEst interface only exists in my forked CUTEst repository, and compiling it on a Mac 
with MPI compilers requires my forked and modified ARCHDEFS repo. These changes may be contributed to the main repos 
as pull requests at a future date.

### Usage Examples

Test bound-constrained Conjugate Gradient method (`bncg`) with the Hager-Zhang update (`-tao_bncg_type hz`) on the bound-constrained problem set and save results into `bound/bncg/hz`:
`python2 profile.py --probtype bound --solver bncg --flags "-tao_bncg_type hz" --outdir bncg/hz`

Test bound-constrained quasi-Newton line-search (`bqnls`) with history size of 25 on the unconstrained problem set and save results into `uncon/bqnls/q25`:
`python2 profile.py --probtype uncon --solver bqnls --flags "-tao_bqnls_mat_lmvm_num_vecs 25" --outdir bqnls/q25`

### Mass Testing
1. Run `profile.py` with the -b flag to save executables for future steps. Specify the problem-type directory with -p <dirname>. Examples of what these should look like are the bound, uncon, and bound_and_uncon directories. Official results are with the latter directory.
2. Run `execs_run.sh <probdir>` to loop over many parameters and wait a few weeks for results. Alternatively, one may run `execs_run_short.sh <probdir>` to generate a select number of performance profiles.
3. Run `auto_parse.py -p <probdir> -o <outname> -s <result_dirs>`. For example, -s bncg bqnls would look for the output files in <probdir>/bncg/<subdirectories> and <probdir>/bqnls/<subdirectories>.
4. (If one has only a few results to plot) Run `perfplot.py -p <probdir> -s <outname>` where <outname> is the output name from the previous step. (If one has generated a large number of output files) Run `integral_sieve.py -p <probdir> -s <outname>` to obtain the best-performing methods. If one wishes to see their performance profiles in a plot, go back to step 3 and only include the <result_dirs> that correspond to the results obtained.

